﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace VersionCheckApp
{
    //Utils class
    class Util
    {
                //Comparing versions
        public void CheckVersion ()
        {
            DependencyService.Get<IInfoService>().CheckVersions();
        }

    }
}
