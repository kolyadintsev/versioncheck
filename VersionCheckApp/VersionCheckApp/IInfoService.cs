﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionCheckApp
{
    //Interface for different OS
    public interface IInfoService
    {
        void CheckVersions();
    }
}
