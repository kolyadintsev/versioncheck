﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace VersionCheckApp
{
    public partial class StartPage : ContentPage
    {
        public StartPage()
        {
            InitializeComponent();
        }

        void OnButtonClicked (object sender, EventArgs e)
        {
           new Util().CheckVersion();
        }
    }
}
