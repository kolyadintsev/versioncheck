using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using System.Net;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Net.Http;

[assembly: Xamarin.Forms.Dependency(typeof(VersionCheckApp.Droid.InfoService))]

namespace VersionCheckApp.Droid
{
    // Interface implementation with Android implementation
    class InfoService : IInfoService
    {

        string PackageName
        {
            get
            {
                return Forms.Context.PackageName;
            }
        }

        //Get App version
        int[] AppVersionName
        {
            get
            {
                var context = Forms.Context;
                var version = context.PackageManager.GetPackageInfo(context.PackageName, 0).VersionName;
                Char dot = '.';
                String[] substrings = version.Split(dot);
                List<int> result = new List<int>();
                for (int i = 0; i < substrings.Length; i++)
                {
                    result.Add(int.Parse(substrings[i]));
                }
                return result.ToArray();
            }
        }

        int AppVersionCode
        {
            get
            {
                var context = Forms.Context;
                return context.PackageManager.GetPackageInfo(context.PackageName, 0).VersionCode;
            }
        }

        public async void CheckVersions()
        {
            var webClient = new WebClient();

            webClient.DownloadStringCompleted += WebClient_DownloadStringCompleted;
            await webClient.DownloadStringTaskAsync(new Uri("https://s3.amazonaws.com/kolyadintsevtest/filecheckerversion.txt"));

        }

        private void WebClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {            
            Char dot = '.';
            String[] substrings = e.Result.Split(dot);
            List<int> result = new List<int>();
            for (int i = 0; i < substrings.Length; i++)
            {
                result.Add(int.Parse(substrings[i]));
            }
            CompareVersions(result.ToArray());
        }

        private void CompareVersions(int[] serverVersion) 
        {
            var currentVersion = AppVersionName;
            var min = Math.Min(currentVersion.Length, serverVersion.Length);
            bool result = false;

            //Comparing version number from major to minor number
            for (int i = 0; i < min; i++)
            {
                //if current version is newer
                if (currentVersion[i] > serverVersion[i])
                {
                    result = false;
                    break;
                }
                else
                {
                    //if server version is newer
                    if (currentVersion[i] < serverVersion[i])
                    {
                        result = true;
                        break;
                    }
                }
            }
            //nothing found, i.e. versions are the same

            if (result)
            {
                Toast.MakeText(Forms.Context, "New app version will be installed now", ToastLength.Long).Show();
                DownloadNewApk();
            }
            else
            {
                Toast.MakeText(Forms.Context, "Your app version is latest", ToastLength.Long).Show();
            }
        }

        private void DownloadNewApk()
        {
            var webClient = new WebClient();

            webClient.DownloadFileCompleted += (s, e) =>
            {  
                //If no error
                if (e.Error == null)
                {
                    try
                    {
                        //Install app with root rights                     

                        var filename = Android.OS.Environment.ExternalStorageDirectory + "/download/" + "File.apk";
                        var command = "adb install -r " + filename;
                        Java.Lang.Process proc = Java.Lang.Runtime.GetRuntime().Exec(new String[] { "su", "-c", command });
                        proc.WaitFor();
                        
                    }
                    catch (Exception ex)
                    {
                        //If error (or no root rights)
                        try
                        {
                            //Install app with prompt
                            Intent promptInstall = new Intent(Intent.ActionInstallPackage).SetDataAndType(Android.Net.Uri.FromFile(new Java.IO.File(Android.OS.Environment.ExternalStorageDirectory + "/download/" + "File.apk")), "application/vnd.android.package-archive");
                            promptInstall.AddFlags(ActivityFlags.NewTask);
                            promptInstall.AddFlags(ActivityFlags.GrantReadUriPermission);
                            var context = Forms.Context;
                            Forms.Context.StartActivity(promptInstall);
                        }
                        catch (ActivityNotFoundException ex1)
                        {
                            //.apk install error
                            Toast.MakeText(Forms.Context, "Apk file install error", ToastLength.Long).Show();
                        }

                    }

                }
            };

            var url = new System.Uri("https://s3.amazonaws.com/kolyadintsevtest/file.apk");
            webClient.DownloadFileAsync(url, Android.OS.Environment.ExternalStorageDirectory + "/download/File.apk");
        }
        
    }
}