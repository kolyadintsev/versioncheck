﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Threading.Tasks;
using Android;
using Android.Support.V4.Content;
using Android.Support.V4.App;

namespace VersionCheckApp.Droid
{
    [Activity(Label = "VersionCheckApp", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());

            var t = Task.Run(() => GetPermissionsCompatAsync());           

        }

        //Ask for permissions
        async Task GetPermissionsCompatAsync()
        {
            string[] PermissionsLocation =
    {
            Manifest.Permission.AccessNetworkState,
            Manifest.Permission.Internet,
            Manifest.Permission.ReadPhoneState,
            Manifest.Permission.WriteExternalStorage,
            Manifest.Permission.InstallPackages,
            Manifest.Permission.RequestInstallPackages
        };

            ActivityCompat.RequestPermissions(this, PermissionsLocation, 0);
        }


    }
}

